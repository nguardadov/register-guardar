package com.example.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

class MperfilActivity : AppCompatActivity() {

    lateinit var uid: String
    private lateinit var mDataBaseReference: DatabaseReference
    private  lateinit var mDatabase: FirebaseDatabase
    private lateinit var auth: FirebaseAuth

    private lateinit var tvname : TextView
    private lateinit var tvemail : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mperfil)
        initialise();
    }

    private fun initialise()
    {
        auth = FirebaseAuth.getInstance()
        val user: FirebaseUser?=auth.currentUser
        //para los textos
        tvname = findViewById(R.id.tvusername)
        tvemail = findViewById(R.id.tvemail)

        //agregando el email
        tvemail.text = user?.email!!.toString()

        //buscando el nombre
        val ref = FirebaseDatabase.getInstance().getReference("Users")
        val userRef = ref.child(user?.uid!!)

        tvname.text = user?.uid!!.toString()
        /*userRef.addValueEventListener(object:ValueEventListener{
            override fun onCancelled(dataSnapshot: DatabaseError) {
                Log.d("tag", "ENTRO ACA");
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {

               tvname.text = dataSnapshot.child("nombre").value as String
            }

        })*/

    }
}


