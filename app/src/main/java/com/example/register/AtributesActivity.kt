package com.example.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.example.register.Data.Disciplina
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_atributes.*

class AtributesActivity : AppCompatActivity() {

    lateinit var txtEducacion:EditText
    lateinit var txtOcupacion:EditText
    lateinit var txtDescripcion:EditText
    lateinit var  listaDisciplina:ArrayList<Disciplina>
    lateinit var  btnGuardar:Button

    //check box
    lateinit var  cbdis1 : CheckBox
    lateinit var  cbdis2 : CheckBox
    lateinit var  cbdis3 : CheckBox
    lateinit var  cbdis4 : CheckBox

    lateinit var uid:String
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_atributes)

        initialize()



        initArrayDisciplina()
        cbdis1.setOnCheckedChangeListener{ _, _ ->
            when {
                cbdis1.isChecked -> {
                    this.listaDisciplina[0].estado = true
                }
                else -> {
                    this.listaDisciplina[0].estado = false
                }
            }
        }

        cbdis2.setOnCheckedChangeListener{ _, _ ->
            when {
                cbdis2.isChecked -> {
                    this.listaDisciplina[1].estado = true
                }
                else -> {
                    this.listaDisciplina[1].estado = false
                }
            }
        }

        cbdis3.setOnCheckedChangeListener{ _, _ ->
            when {
                cbdis3.isChecked -> {
                    this.listaDisciplina[2].estado = true
                }
                else -> {
                    this.listaDisciplina[2].estado = false
                }
            }
        }

        cbdis4.setOnCheckedChangeListener{ _, _ ->
            when {
                cbdis4.isChecked -> {
                    this.listaDisciplina[3].estado = true
                }
                else -> {
                    this.listaDisciplina[3].estado = false
                }
            }
        }


        guardar_tutor.setOnClickListener {
          // startActivity(Intent(this,MperfilActivity::class.java))
            guardar()
       }
    }


    private fun initArrayDisciplina()
    {
        this.listaDisciplina = ArrayList();
        this.listaDisciplina.add(Disciplina("1","Disciplina1","",false))
        this.listaDisciplina.add(Disciplina("2","Disciplina2","",false))
        this.listaDisciplina.add(Disciplina("3","Disciplina3","",false))
        this.listaDisciplina.add(Disciplina("4","Disciplina4","",false))
    }

    private fun initialize(){
        this.txtEducacion = findViewById(R.id.educacion_tutor)
        this.txtOcupacion = findViewById(R.id.ocupacion_tutor)
        this.txtDescripcion = findViewById(R.id.descripcion_tutor)
        this.cbdis1 = findViewById(R.id.cbdis1)
        this.cbdis2 = findViewById(R.id.cbdis2)
        this.cbdis3 = findViewById(R.id.cbdis3)
        this.cbdis4 = findViewById(R.id.cbdis4)
        this.btnGuardar = findViewById(R.id.guardar_tutor)

        auth=FirebaseAuth.getInstance()
        val user: FirebaseUser?=auth.currentUser
        uid = user?.uid!!

        Log.d("Uid-Usuario", uid.toString())
        //this.txtEducacion.text = uid.toString()
    }


    private fun guardar()
    {
        val referencia = FirebaseDatabase.getInstance().getReference("Users").child(uid)
        referencia.child("nivel").setValue(txtEducacion.text.toString())
        referencia.child("ocupacion").setValue(txtOcupacion.text.toString())
        referencia.child("Descripcion").setValue(txtDescripcion.text.toString())
        referencia.child("disciplinas").setValue(listaDisciplina)

        Toast.makeText(this,"Guardado con exito",Toast.LENGTH_LONG).show()

    }








}
